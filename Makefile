all:
	pdflatex -halt-on-error LIVRO.tex
	pdflatex -halt-on-error LIVRO.tex

dub:
	pdflatex LIVRO.tex	
	pdflatex LIVRO.tex
	evince LIVRO.pdf
clean:
	rm *.aux *.log LIVRO.pdf *.toc *.out
